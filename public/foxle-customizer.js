const LAYERS = ["Body", "Body Shadows", "Markings", "Markings Shadows", "Belly", "Eyes", "Eyes Highlight", "Nose", "Hearts"];

const PRESETS = {
    "Classic":      ["#D87644", "#BE4A2F", "#743F39", "#3F2832", "#EAD4AA", "#3F2832", "#FFFFFF", "#3F2832", "#F6757A"],
    "Dawnbringer":  ["#DF7126", "#8F563B", "#663931", "#45283C", "#EEC39A", "#45283C", "#FFFFFF", "#45283C", "#D95763"],
    "Coffee":       ["#B86F50", "#743F39", "#EAD4AA", "#E4A672", "#3F2832", "#181425", "#FFFFFF", "#181425", "#F6757A"],
    "Flame":        ["#FEAE34", "#F77622", "#E43B44", "#9E2835", "#FEE761", "#9E2835", "#FFFFFF", "#9E2835", "#124E89"],
    "Frost":        ["#8B9BB4", "#5A6988", "#3A4466", "#262B44", "#C0CBDC", "#262B44", "#FFFFFF", "#262B44", "#F6757A"],
    "Sublime":      ["#FD971F", "#F92672", "#66D9EF", "#AE81FF", "#E6DB74", "#A6E22E", "#F8F8F2", "#75715E", "#1B1D1E"],
    "Watermelon":   ["#E8B796", "#C28569", "#265C42", "#193C3E", "#F6757A", "#193C3E", "#FFFFFF", "#193C3E", "#63C74D"],
};

/** @type {HTMLInputElement[]} */
let swatchInputs = [];

/** @type {HTMLCanvasElement} */
const renderCanvas = document.createElement("canvas");
renderCanvas.width = 2560;
renderCanvas.height = 160;

/** @type {string} */
let spriteSheet;

/** @type {HTMLAnchorElement} */
const downloadSpriteSheetLink = document.getElementById("downloadSpriteSheet");

/** @type {HTMLDivElement} */
const preview = document.getElementById("preview");
preview.onclick = () => {
    if (preview.classList.contains("heart")) {
        return;
    }

    preview.classList.add("heart");
    window.setTimeout(() => preview.classList.remove("heart"), 1600);
};

/** @type {HTMLImageElement} */
const gifPreview = document.getElementById("gif");

/**
 * Update the color pickers to have the values set by the named preset.
 * @param {string} preset 
 */
function applyPreset(preset) {
    const colors = PRESETS[preset];

    for (let i = 0; i < LAYERS.length; i++) {
        swatchInputs[i].value = colors[i];
        localStorage[LAYERS[i]] = colors[i];
        colorLayer(LAYERS[i], colors[i]);
    }
}

/**
 * Load and apply color to a layer.
 * @param {string} layer - the name of the layer to load and color
 * @param {string} color - the hex string of the color to apply
 */
async function colorLayer(layer, color) {
    // Convert input to RGB

    if (color.startsWith("#")) {
        color = color.substring(1);
    }

    if (color.length !== 6) {
        throw new Error(`Invalid color string '${color}' passed to colorToRGB.`);
    }

    const r = parseInt(color.substring(0, 2), 16);
    const g = parseInt(color.substring(2, 4), 16);
    const b = parseInt(color.substring(4, 6), 16);

    // Load the layer image

    const layerCanvas = document.createElement("canvas");
    const layerContext = layerCanvas.getContext("2d");
    const layerImage = new Image();

    await new Promise((resolve, _) => {
        layerImage.onload = () => resolve();
        layerImage.src = `layers/${layer}.png`;
    });

    layerCanvas.width = renderCanvas.width;
    layerCanvas.height = renderCanvas.height;
    layerContext.imageSmoothingEnabled = false;
    layerContext.drawImage(layerImage, 0, 0, layerCanvas.width, layerCanvas.height);
    const layerPixels = layerContext.getImageData(0, 0, layerCanvas.width, layerCanvas.height);

    // Apply layer mask onto current render

    const renderContext = renderCanvas.getContext("2d");
    const renderPixels = renderContext.getImageData(0, 0, renderCanvas.width, renderCanvas.height);
    console.assert(renderPixels.data.length === layerPixels.data.length);

    for (let i = 0; i < layerPixels.data.length; i += 4) {
        if (layerPixels.data[i + 3] === 0) {
            // Transparent pixel
            continue;
        }

        renderPixels.data[i + 0] = r;
        renderPixels.data[i + 1] = g;
        renderPixels.data[i + 2] = b;
        renderPixels.data[i + 3] = 255;
    }

    renderContext.putImageData(renderPixels, 0, 0);
    layerCanvas.remove();

    // Show in preview        
    spriteSheet = renderCanvas.toDataURL();
    downloadSpriteSheetLink.href = spriteSheet;
    preview.style.backgroundImage = `url(${spriteSheet})`;
}

function generateGIF() {
    let images = [];
    
    let canvas = document.createElement("canvas");
    canvas.width = 160;
    canvas.height = 160;
    
    let ctx = canvas.getContext("2d");
    
    let image = new Image();
    image.src = spriteSheet;
    
    let background = new Image();
    background.src = "texture_grass.png";

    for (let f = 0; f < 16; f++) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(background, 0, 0);
        ctx.drawImage(image, -canvas.width * f, 0);
        images.push(canvas.toDataURL());

        if (f === 0) {
            // Repeat first frame
            const frame = images[0];
            for (let i = 1; i < 20; i++) {
                images.push(frame);
            }
        }
    }

    gifshot.createGIF({
        images: images,
        gifWidth: canvas.width,
        gifHeight: canvas.height,
    }, (obj) => {
        if (obj.error) {
            window.alert(obj.error);
        } else {
            gifPreview.src = obj.image;
        }
    });

    canvas.remove();
}

window.onload = function () {
    // Show presets
    const presets = document.getElementById("presets");
    for (let [preset, colors] of Object.entries(PRESETS)) {
        const button = document.createElement("button");
        button.textContent = preset;

        const sampleGroup = document.createElement("div");
        button.append(sampleGroup);

        for (let color of colors) {
            const sample = document.createElement("span");
            sample.style.backgroundColor = color;
            sample.innerHTML = "&emsp;";
            sampleGroup.append(sample);
        }

        button.onclick = () => applyPreset(preset);
        presets.appendChild(button);
    }

    // Show layer options
    const swatches = document.getElementById("swatches");
    for (let layer of LAYERS) {
        const title = document.createElement("span");
        title.textContent = layer;

        const colorPicker = document.createElement("input");
        colorPicker.type = "color";
        colorPicker.oninput = () => {
            localStorage[layer] = colorPicker.value;
            colorLayer(layer, colorPicker.value);
        };
        swatchInputs.push(colorPicker);

        if (typeof window.localStorage[layer] !== "undefined") {
            colorPicker.value = window.localStorage[layer];
        }

        const layerControls = document.createElement("label");
        layerControls.classList.add("swatch");
        layerControls.append(title, colorPicker);
        swatches.appendChild(layerControls);
    }

    // Initialize
    if (window.localStorage["used"]) {
        for (let l = 0; l < LAYERS.length; l++) {
            colorLayer(LAYERS[l], swatchInputs[l].value);
        }
    } else {
        applyPreset("Sublime");
        window.localStorage["used"] = "true";
    }
};
